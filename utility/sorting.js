function sorting(arr) {

  while (true) {
    var sorted = true;
    for (let I = 0; I < arr.length - 1; I++) {
      if (arr[I] > arr[I + 1]) {
        sorted = false;
        var val = arr[I + 1];
        arr[I + 1] = arr[I];
        arr[I] = val;
      }
    }
    if (sorted) {
      break;
    }
  }
  return arr;
}