function printHTML(arr2D, role = true) {
    // arr2D 2 dim
    var table = `<table>`;
    // header
    if (role) {
        table += `<tr>`;
        table += `<td>R/C</td>`;
        for (let I = 0; I < arr2D[0].length; I++) {
            table += `<td class='role'>${I}</td>`;
        }
        table += `</tr>`;
    }
    for (let row = 0; row < arr2D.length; row++) {
        table += `<tr>`;
        if (role) {
            table += `<td class='role'>${row}</td>`;
        }
        for (let col = 0; col < arr2D[row].length; col++) {
            table += `<td>${arr2D[row][col]}</td>`;
        }
        table += `</tr>`;
    }
    table += `</table>`;
    document.getElementById("target").innerHTML = table;
}