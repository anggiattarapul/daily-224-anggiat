function strongpass(pass){
    //0. width >6
    //1 ada lower
    //2 ada upper
    //3 ada number
    //4 antara symbol!@#$%^&*()-+
    var level = [0,0,0,0,0];
    var result = 0;
    if(pass.length >= 6){
        level[0] = 1;
    }
    for (let i=0; i< pass.length;i++){
        if (pass.charCodeAt(i)>=97 && pass.charCodeAt(i) <=122){
            level[1] = 1;
        }
        if (pass.charCodeAt(i)>=65 && pass.charCodeAt(i) <=90){
            level[2] = 1;
        }
        if (pass.charCodeAt(i)>=48 && pass.charCodeAt(i) <=57){
            level[3] = 1;
        }
        if (pass.charCodeAt(i)>=33 && pass.charCodeAt(i) <=43 || pass.charCodeAt(i) == 64 ){
        
            level[4] = 1;
        }
    }
    for (let n = 0; n < level.length; n++) {
        result = parseInt(result) + level[n];
    }
    var arr2d = Array2D(3, 2);
    arr2d =[
        ["Pass", pass],
        ["Strong",result],
        ["Explain", (result == 5 ? "Strong" : "Weak")
    ]
]
   printHTML(arr2d);
}