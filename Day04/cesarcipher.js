function cesarcipher(str){
    var strArr = str.split("");
    for (let i = 0; i <str.length; i++){
        if(strArr[i].charCodeAt(0) >= 97 && strArr[i].charCodeAt(0) <= 120){
            strArr[i] = String.fromCharCode(strArr[i].charCodeAt(0)+2);
        }else if(strArr[i].charCodeAt(0) >= 121 && strArr[i].charCodeAt(0) <= 122){
            strArr[i] = String.fromCharCode(strArr[i].charCodeAt(0)-24);
        }else if(strArr[i].charCodeAt(0) >= 65 && strArr[i].charCodeAt(0) <= 88){
            strArr[i] = String.fromCharCode(strArr[i].charCodeAt(0)+2);
        }else if(strArr[i].charCodeAt(0) >= 89 && strArr[i].charCodeAt(0) <= 90){
            strArr[i] = String.fromCharCode(strArr[i].charCodeAt(0)-24);
        }
    }
    var arr2d = Array2D(2,2);
    arr2d =[
        ["String", str],
        ["Result", strArr.join]
    ];
    printHTML(arr2d, false);

}