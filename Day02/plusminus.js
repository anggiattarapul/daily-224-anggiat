function plusminus(arr){
    var number = arr.split(" ")
    var positive = 0;
    var negative = 0;
    var zero = 0;
    for (let i = 0; i< number.length; i++){
        if(Math.sign(parseInt(number[i])) == 1){
            positive++;
        }
        else if(Math.sign(parseInt(number[i])) == -1){
            negative++;
        }
        else if(Math.sign(parseInt(number[i])) == 0){
            zero++;
        }  
    }

        console.log(`positive : ${positive/number.length}`);
        console.log(`negative : ${negative/number.length}`);
        console.log(`zero : ${zero/number.length}`);
}
