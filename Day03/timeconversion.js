function timeconversion(arr) {
    var hour = arr.split(":");
    var pmOrAm = hour[2].split(/(\d+)/);
    var result;
    var valid = true;
    if (hour[0] == 00 && pmOrAm[2] =="PM"){
        valid = false;
        console.log("invalid time");
    }    
    else if(hour[0] == 13 || hour[1] >= 60||hour[2] >= 60) {
        valid = false;
        console.log("invalid time");
    }
    else if (pmOrAm[2] == "PM") {
        result = parseInt(hour[0]) + 12;
        if (result == 24) {
            result = "00"
        }
        result = result.toString();
        result = result + ":" + hour[1] + ":" + pmOrAm[1];
    } else {
        result = arr.replace(":", ".");
    }
    console.log(result)

}