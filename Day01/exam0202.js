function exam0202(n1, n2){
  var arr = new Array();
  arr[0] = new Array();
  arr[1] = new Array();

  for (let col = 0; col < n1; col++){
    arr[0][col] = col;
    
    if((col+1)%3 == 0){
      arr[1][col] = Math.pow(n2, col)*-1;
    }else{
      arr[1][col] = Math.pow(n2, col);
    }
  }
  printhtml(arr);
}