function exam0209(kalimat) {
  var arrWords = kalimat.split(" ");
  var result = "";
  arrWords.forEach(word => {
    for (let I = 0; I < word.length; I++) {
      if (I === 0) {
        result += word.substring(I, 1);
      } else if (I === (word.length - 1)) {
        result += word.substring(I, word.length);
      } else {
        result += "*";
      }
    }
    result += " ";
  });
  var arr = new Array(2);
  arr[0] = arrWords;
  arr[1] = result;
  printhtml(new Array(arr));
}